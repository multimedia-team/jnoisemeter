// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"


XftColor      *XftColors [NXFTCOLORS];
XftFont       *XftFonts [NXFTFONTS];

X_textln_style tstyle1;
X_textln_style tstyle2;
X_button_style bstyle1;


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG] = disp->alloc_xftcolor (0.95f, 0.91f, 0.87f, 1.0f);
    XftColors [C_MAIN_FG] = disp->alloc_xftcolor (0.00f, 0.00f, 0.00f, 1.0f);
    XftColors [C_MAIN_LS] = disp->alloc_xftcolor (1.00f, 1.00f, 1.00f, 1.0f);
    XftColors [C_MAIN_DS] = disp->alloc_xftcolor (0.20f, 0.20f, 0.20f, 1.0f);
    XftColors [C_DISP_BG] = disp->alloc_xftcolor (0.10f, 0.10f, 0.10f, 1.0f);
    XftColors [C_DISP_FG] = disp->alloc_xftcolor (1.00f, 1.00f, 0.00f, 1.0f);
    XftColors [C_BUTT_B0] = disp->alloc_xftcolor (0.91f, 0.87f, 0.83f, 1.0f);
    XftColors [C_BUTT_F0] = disp->alloc_xftcolor (0.00f, 0.00f, 0.00f, 1.0f);
    XftColors [C_BUTT_B1] = disp->alloc_xftcolor (0.20f, 1.00f, 0.20f, 1.0f);
    XftColors [C_BUTT_F1] = disp->alloc_xftcolor (0.00f, 0.00f, 0.00f, 1.0f);
    XftColors [C_BUTT_B2] = disp->alloc_xftcolor (1.00f, 1.00f, 0.00f, 1.0f);
    XftColors [C_BUTT_F2] = XftColors [C_BUTT_F1];

    XftFonts [F_TEXT] = disp->alloc_xftfont (xrm->get (".font.text", "luxi:bold:pixelsize=11"));
    XftFonts [F_BUTT] = disp->alloc_xftfont (xrm->get (".font.butt", "luxi:bold:pixelsize=11"));
    XftFonts [F_DISP] = disp->alloc_xftfont (xrm->get (".font.disp", "fira:bold:pixelsize=25"));

    tstyle1.font = XftFonts [F_TEXT];
    tstyle1.color.normal.bgnd = XftColors [C_MAIN_BG]->pixel;
    tstyle1.color.normal.text = XftColors [C_MAIN_FG];

    tstyle2.font = XftFonts [F_DISP];
    tstyle2.color.normal.bgnd = XftColors [C_DISP_BG]->pixel;
    tstyle2.color.normal.text = XftColors [C_DISP_FG];
    tstyle2.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    tstyle2.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;

    bstyle1.font = XftFonts [F_BUTT];
    bstyle1.type = X_button_style::RAISED;
    bstyle1.color.bg[0] = XftColors [C_BUTT_B0]->pixel;
    bstyle1.color.fg[0] = XftColors [C_BUTT_F0];
    bstyle1.color.bg[1] = XftColors [C_BUTT_B1]->pixel;
    bstyle1.color.fg[1] = XftColors [C_BUTT_F1];
    bstyle1.color.bg[2] = XftColors [C_BUTT_B2]->pixel;
    bstyle1.color.fg[2] = XftColors [C_BUTT_F2];
    bstyle1.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;
    bstyle1.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    bstyle1.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;
}


void styles_fini (X_display *disp)
{
}
