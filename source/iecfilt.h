// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2023 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __IECFILT_H
#define __IECFILT_H

#include "bp6filter.h"


extern Bp6paramset Oct1filt44;
extern Bp6paramset Oct1filt48;
extern Bp6paramset Oct1filt88;
extern Bp6paramset Oct1filt96;
extern Bp6paramset Oct1filt192;


extern Bp6paramset Oct3filt44;
extern Bp6paramset Oct3filt48;
extern Bp6paramset Oct3filt88;
extern Bp6paramset Oct3filt96;
extern Bp6paramset Oct3filt192;


#endif
