// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2023 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GLOBAL_H
#define __GLOBAL_H


#define  PROGNAME "jnoisemeter"

enum { EV_X11 = 16, EV_EXIT = 31 };
enum { DCF_OFF = 0, DCF_5HZ = 1 };
enum { FIL_NONE, FIL_20KHZ, FIL_IECA, FIL_IECC, FIL_ITU1, FIL_ITU2, FIL_OCT };
enum { DET_ITU, DET_RMS, DET_VUM };
enum { ST_INI = 0, ST_ACT = 1, ST_ERR = -1 };

#endif
