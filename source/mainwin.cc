// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2023 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "styles.h"
#include "global.h"
#include "mainwin.h"


static const char *inp_text [] = { "1", "2", "3", "4", "5", "6", "7", "8" };
static const char *fil_text [] = { "Flat", "20k", "A", "C", "ITU", "ITU2",
				   "32", "63", "125", "250", "500",
				   "1k", "2k", "4k", "8k", "16k" };
static const char *det_text [] = { "ITU", "RMS", "AV" };



Mainwin::Mainwin (X_rootwin *parent, X_resman *xres, int xp, int yp, Jclient *jclient) :
    A_thread ("Main"),
    X_window (parent, xp, yp, XSIZE, YSIZE, XftColors [C_MAIN_BG]->pixel),
    _stop (false),
    _xres (xres),
    _jclient (jclient),
    _input (0),
    _filter (0),
    _detect (0)
{
    X_hints     H;
    char        s [1024];
    int         i, x, y;

    _atom = XInternAtom (dpy (), "WM_DELETE_WINDOW", True);
    XSetWMProtocols (dpy (), win (), &_atom, 1);
    _atom = XInternAtom (dpy (), "WM_PROTOCOLS", True);

    sprintf (s, "%s - %s  [%s]", PROGNAME, VERSION, jclient->jname ());
    x_set_title (s);
    H.position (xp, yp);
    H.minsize (XSIZE, YSIZE);
    H.maxsize (XSIZE, YSIZE);
    H.rname (xres->rname ());
    H.rclas (xres->rclas ());
    x_apply (&H); 

    _t_disp = new X_textip (this, this, &tstyle2, 200, 2, 195, 43, 31);
    _t_disp->set_align (1);
    _t_disp->x_map ();
    if (_jclient->state () == ST_ERR) _t_disp->set_text ("Err: FS ");

    bstyle1.size.x = 24;
    bstyle1.size.y = 17;
    x = 70;
    y = 6;
    (new X_textln (this, &tstyle1, 10, y, 55, 17, "Input", 0))->x_map ();
    for (i = 0; i < 8; i++)
    {
        _b_input [i] = new X_tbutton (this, this, &bstyle1, x, y, inp_text [i], 0, B_INP + i);
        _b_input [i]->x_map ();
	if (i == 3)
	{
            x = 70;
	    y += 18;
	}
	else x += 25;
    }

    bstyle1.size.x = 44;
    bstyle1.size.y = 17;
    x = 70;
    y = 52;
    (new X_textln (this, &tstyle1, 10, y, 55, 17, "Filter", 0))->x_map ();
    _b_dcfilt = new X_tbutton (this, this, &bstyle1, x, y, "DC", 0, B_DCF);
    _b_dcfilt->x_map ();
    x += 50;
    for (i = 0; i < 16; i++)
    {
        _b_filter [i] = new X_tbutton (this, this, &bstyle1, x, y, fil_text [i], 0, B_FIL + i);
        _b_filter [i]->x_map ();
	if ((i == 5) || (i == 10))
	{
	    x -= 180;
	    y += 19;
	}
	else x += 45;
    }

    x = 70;
    y += 23;
    (new X_textln (this, &tstyle1, 10, y, 55, 17, "Detect", 0))->x_map ();
    _b_slower = new X_tbutton (this, this, &bstyle1, x, y, "Slow", 0, B_SLO);
    _b_slower->x_map ();
    x += 50;
    for (i = 0; i < 3; i++)
    {
        _b_detect [i] = new X_tbutton (this, this, &bstyle1, x, y, det_text [i], 0, B_DET + i);
        _b_detect [i]->x_map ();
        x += 45;
    }
    
    set_input (0);
    set_filter (FIL_NONE);
    set_detect (DET_RMS);

    x_add_events (ExposureMask); 
    x_map (); 
    set_time (0);
    inc_time (200000);
}

 
Mainwin::~Mainwin (void)
{
}

 
int Mainwin::process (void)
{
    int e;

    if (_stop) handle_stop ();

    e = get_event_timed ();
    switch (e)
    {
    case EV_TIME:
        handle_time ();
	break;
    }
    return e;
}


void Mainwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case Expose:
	expose ((XExposeEvent *) E);
	break;  
 
    case ClientMessage:
        clmesg ((XClientMessageEvent *) E);
        break;
    }
}


void Mainwin::expose (XExposeEvent *E)
{
    if (E->count) return;
    redraw ();
}


void Mainwin::clmesg (XClientMessageEvent *E)
{
    if (E->message_type == _atom) _stop = true;
}



void Mainwin::handle_time (void)
{
    float v;
    char  s [16];

    if (_jclient->state () == ST_ERR) return;
    v = _jclient->get_value ();
    if (v < 1e-10f) 
    {
        _t_disp->set_text ("--    ");
    }
    else
    {
        sprintf (s, "%+3.1lf ", 20 * log10f (v));
        _t_disp->set_text (s);
    }
    inc_time (200000);
    XFlush (dpy ());
}


void Mainwin::handle_stop (void)
{
    put_event (EV_EXIT, 1);
}


void Mainwin::handle_callb (int type, X_window *W, XEvent *E)
{
    X_button *B;
    int       k;

    if (type == (X_callback::BUTTON | X_button::PRESS))
    {
	B = (X_button *) W;
	k = B->cbid ();
	if (k < B_FIL)
	{
	    set_input (k - B_INP);
	    return;
	}
	if (k < B_DET)
	{
	    set_filter (k - B_FIL);
	    return;
	}
	if (k < B_DCF)
	{
	    set_detect (k - B_DET);
	    return;
	}
	if (k == B_DCF)
	{
	    B->set_stat (B->stat () ^ 2);
	    _jclient->set_dcfilt (B->stat ());
	    return;
	}
	if (k == B_SLO)
	{
	    B->set_stat (B->stat () ^ 2);
	    _jclient->set_slower (B->stat ());
	    return;;
	}
    }
}


void Mainwin::set_input (int k)
{
    _b_input [_input]->set_stat (0);
    _input = k;
    _b_input [_input]->set_stat (1);
    _jclient->set_input (_input);
}


void Mainwin::set_filter (int k)
{
    _b_filter [_filter]->set_stat (0);
    _filter = k;
    _b_filter [_filter]->set_stat (1);
    _jclient->set_filter (_filter);
}


void Mainwin::set_detect (int k)
{
    _b_detect [_detect]->set_stat (0);
    _detect = k;
    _b_detect [_detect]->set_stat (1);
    _jclient->set_detect (_detect);
    if (k == DET_ITU) _b_slower->set_stat (0);
}


void Mainwin::redraw (void)
{
}


