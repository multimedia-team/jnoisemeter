// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2023 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <string.h>
#include "jclient.h"
#include "global.h"


Jclient::Jclient (const char *jname, const char *jserv) :
    A_thread ("Jclient"),
    _jack_client (0),
    _jname (0),
    _state (ST_INI),
    _input (0),
    _dcfilt (-1),
    _filter (-1),
    _detect (-1),
    _slower (false)
{
    init_jack (jname, jserv);
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
}


void Jclient::init_jack (const char *jname, const char *jserv)
{
    int            i;
    char           s [16];
    int            opts;
    jack_status_t  stat;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    _jack_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv);
    if (! _jack_client)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        exit (1);
    }

    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);
    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }
    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);

    switch (_fsamp)
    {
    case 44100:
	_oct1par = &Oct1filt44;
	break;
    case 48000:
	_oct1par = &Oct1filt48;
	break;
    case 88200:
	_oct1par = &Oct1filt88;
	break;
    case 96000:
	_oct1par = &Oct1filt96;
	break;
    default:
	_state = ST_ERR;
	_oct1par = 0;
	
    }	

    for (i = 0; i < 8; i++)
    {
	sprintf (s, "in_%d", i + 1);
        _inpports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    }
    _outports [0] = jack_port_register (_jack_client, "out", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    _state = ST_ACT;
}


void Jclient::close_jack ()
{
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    return ((Jclient *) arg)->jack_shutdown ();
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}


void Jclient::set_dcfilt (int dcfilt)
{
    if (_dcfilt == dcfilt) return;
    _dcw = 5 * 6.283f / _fsamp;
    _dcz = 0.0f;
    _dcfilt = dcfilt;
}


void Jclient::set_input (int input)
{
    if (_input == input) return;
    switch (_detect)
    {
    case DET_RMS:
	_rmsdetect.reset ();
	break;
    case DET_ITU:
	_itudetect.reset ();
	break;
    case DET_VUM:
	_vumdetect.reset ();
	break;
    }
    _input = input;
}


void Jclient::set_filter (int filter)
{
    if (_filter == filter) return;
    if (filter >= FIL_OCT)
    {
	if (_oct1par) _bp6filter.setparam (&_oct1par->_param [filter - FIL_OCT]);
	else          _bp6filter.setparam (0);
    }
    else
    {
	switch (filter)
	{
	case FIL_20KHZ:
	    _lpefilter.init (_fsamp);
	    break;
	case FIL_IECA:
	case FIL_IECC:
	    _acwfilter.init (_fsamp);
	    break;
	case FIL_ITU1:
	    _itufilter.init (_fsamp, false);
	    break;
	case FIL_ITU2:
	    _itufilter.init (_fsamp, true);
	    break;
	}
    }
    _filter = filter;
}


void Jclient::set_detect (int detect)
{
    if (_detect == detect) return;
    switch (detect)
    {
    case DET_VUM:
        _vumdetect.init (_fsamp);
	break;
    case DET_RMS:
	_rmsdetect.init (_fsamp);
	break;
    case DET_ITU:
	_itudetect.init (_fsamp);
	break;
    }
    _detect = detect;
    set_slower (_slower);
}


void Jclient::set_slower (bool slower)
{
    switch (_detect)
    {
    case DET_VUM:
        _vumdetect.speed (slower);
	break;
    case DET_RMS:
	_rmsdetect.speed (slower);
	break;
    }
    _slower = slower;
}


int Jclient::jack_process (int frames)
{
    int   i;
    float x, z;
    float *inp;
    float *out;

    if (_state == ST_INI) return 0;

    inp = (float *) jack_port_get_buffer (_inpports [_input], frames);
    out = (float *) jack_port_get_buffer (_outports [0], frames);

    if (_dcfilt > 0)
    {
	z = _dcz;
	for (i = 0; i < frames; i++)
	{
	    x = inp [i] + 1e-20f;
	    z += _dcw * (x - z);
	    out [i] = x - z;
	}
	_dcz = z;
    }
    else
    {
	memcpy (out, inp, frames * sizeof (float));
    }

    if (_filter >= FIL_OCT)
    {
	_bp6filter.process (frames, out, out);
    }
    else
    {
	switch (_filter)
	{
	case FIL_20KHZ:
	    _lpefilter.process (frames, out, out);
	    break;
	case FIL_IECA:
	    _acwfilter.process (frames, out, out, 0);
	    break;
	case FIL_IECC:
	    _acwfilter.process (frames, out, 0, out);
	    break;
	case FIL_ITU1:
	case FIL_ITU2:
	    _itufilter.process (frames, out, out);
	    break;
	}
    }
    
    switch (_detect)
    {
    case DET_ITU:
	_itudetect.process (frames, out);
	_val = _itudetect.value ();
	break;
    case DET_RMS:
	_rmsdetect.process (frames, out);
	_val = _rmsdetect.value ();
	break;
    case DET_VUM:
	_vumdetect.process (frames, out);
	_val = _vumdetect.value ();
	break;
    }

    return 0;
}


